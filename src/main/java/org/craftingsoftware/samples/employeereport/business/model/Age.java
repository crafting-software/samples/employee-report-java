package org.craftingsoftware.samples.employeereport.business.model;

public record Age(int value) {
    public static Age of(final int value) {
        return new Age(value);
    }

    public boolean isAdult() {
        return value >= 18;
    }
}
