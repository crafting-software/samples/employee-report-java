package org.craftingsoftware.samples.employeereport.business.model;

import static org.apache.commons.text.WordUtils.capitalizeFully;

public record Name(String value) {
    public static Name none() {
        return new Name("");
    }

    public static Name of(final String value) {
        return new Name(value);
    }

    public Name uppercase() {
        return new Name(value.toUpperCase());
    }

    public Name capitalize() {
        return new Name(capitalizeFully(value));
    }
}
