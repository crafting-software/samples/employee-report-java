package org.craftingsoftware.samples.employeereport.business.usecase;

import io.vavr.collection.List;
import org.craftingsoftware.samples.employeereport.business.model.Employee;

public class ListAdultEmployee {
    private final EmployeeProvider provider;

    public ListAdultEmployee(final EmployeeProvider provider) {
        this.provider = provider;
    }

    List<Employee> list() {
        return provider
                .all()
                .filter(Employee::isAdult)
                .sortBy(employee -> employee.name().value())
                .map(employee -> employee.rename(employee.name().uppercase()));
    }
}
