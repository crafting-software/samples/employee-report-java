package org.craftingsoftware.samples.employeereport.business.model;

public record Employee(Name name, Age age) {
    public static Employee with(final Name name, final Age age) {
        return new Employee(name, age);
    }

    public Employee rename(final Name name) {
        return new Employee(name, age);
    }

    public boolean isAdult() {
        return age.isAdult();
    }
}
