package org.craftingsoftware.samples.employeereport.business.usecase;

import io.vavr.collection.List;
import org.craftingsoftware.samples.employeereport.business.model.Employee;

public interface EmployeeProvider {
    List<Employee> all();
}
