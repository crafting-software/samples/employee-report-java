package org.craftingsoftware.samples.employeereport.business.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NameShould {
    @Test
    public void return_uppercase_name() {
        assertEquals("PAUL", Name.of("Paul").uppercase().value());
    }

    @Test
    public void return_capitalised_name() {
        assertEquals("Paul", Name.of("PAUL").capitalize().value());
    }
}