package org.craftingsoftware.samples.employeereport.business.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmployeeShould {
    @Test
    public void return_true_for_isAdult_when_age_greater_or_equal_18() {
        assertTrue(Employee.with(Name.none(), Age.of(18)).isAdult());
    }

    @Test
    public void return_false_for_isAdult_when_age_less_than_18() {
        assertFalse(Employee.with(Name.none(), Age.of(17)).isAdult());
    }

}