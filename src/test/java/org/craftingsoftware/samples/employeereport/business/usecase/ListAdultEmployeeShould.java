package org.craftingsoftware.samples.employeereport.business.usecase;

import com.google.common.base.Joiner;
import io.vavr.collection.List;
import org.craftingsoftware.samples.employeereport.business.model.Age;
import org.craftingsoftware.samples.employeereport.business.model.Employee;
import org.craftingsoftware.samples.employeereport.business.model.Name;
import org.junit.jupiter.api.Test;

import static java.lang.Integer.parseInt;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ListAdultEmployeeShould {
    @Test
    void select_nothing_when_no_employee() {
        assertEquals(listAdultEmployees(), "");
    }

    @Test
    void select_only_adult_employees() {
        assertEquals(listAdultEmployees("Paul: 32"), "Paul");
        assertEquals(listAdultEmployees("Paul: 32", "Jean: 20"), "Jean, Paul");
        assertEquals(listAdultEmployees("Paul: 32", "Jean: 16"), "Paul");
    }

    private String listAdultEmployees(final String... employees) {
        return Joiner.on(", ").join(adultEmployees(employees).sorted());
    }

    @Test
    void order_employees_by_names() {
        assertEquals(
                "Antoine, Jean, Paul",
                orderOfAdultEmployees("Paul: 32", "Jean: 20", "Antoine: 50", "Mike: 17"));
    }

    private String orderOfAdultEmployees(final String... employees) {
        return Joiner.on(", ").join(adultEmployees(employees));
    }


    @Test()
    void capitalize_employees() {
        assertEquals(
                "ANTOINE, JEAN, PAUL",
                renderOfAdultEmployees("Paul: 32", "Jean: 20", "Antoine: 50", "Mike: 17"));
    }

    private String renderOfAdultEmployees(final String... employees) {
        return Joiner.on(", ").join(
                new ListAdultEmployee((with(employees)))
                        .list()
                        .map(v -> v.name().value())
                        .sorted());
    }

    private List<String> adultEmployees(String[] employees) {
        return new ListAdultEmployee((with(employees)))
                .list()
                .map(employee -> employee.name().capitalize().value());
    }

    private EmployeeProvider with(final String... employees) {
        return () -> List.of(employees).map(this::parse);
    }

    private Employee parse(final String value) {
        final String[] tokens = value.split(":");
        return Employee.with(Name.of(tokens[0].strip()), Age.of(parseInt(tokens[1].strip())));
    }
}